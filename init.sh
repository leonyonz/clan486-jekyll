#!/bin/bash

source /etc/profile.d/rvm.sh
cd /var/www/jekyll
/usr/local/rvm/gems/ruby-2.7.0/bin/jekyll serve
